from django import forms

class LogIn_Form(forms.Form):
    username = forms.CharField(label = "Name", required = True, widget = forms.TextInput(attrs={'id' : 'username_input'}) 
    password = forms.CharField(label = "Password", required = True, widget=forms.PasswordInput(attrs={'id' : 'password_input'})