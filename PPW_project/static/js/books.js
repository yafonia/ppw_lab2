$('.ml11 .letters').each(function(){
  $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letters'>$&</span>"));
});

anime.timeline({loop: true})
  .add({
    targets: '.ml11 .line',
    scaleY: [0,1],
    opacity: [0.5,1],
    easing: "easeOutExpo",
    duration: 700
  })
  .add({
    targets: '.ml11 .line',
    translateX: [0,$(".ml11 .letters").width()],
    easing: "easeOutExpo",
    duration: 700,
    delay: 100
  }).add({
    targets: '.ml11 .letters',
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 600,
    offset: '-=775',
    delay: function(el, i) {
      return 34 * (i+1)
    }
  }).add({
    targets: '.ml11',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 1000
  });

//   $(document).ready(function(){
//     $(".blank-star").click(function(){
//       $(".blank-star").attr('src', 'https://66.media.tumblr.com/d2af96f176296ee2c797e65178353f1f/tumblr_pi1e6qLamv1wct16uo1_540.png');
//     }) 
// })

// var booksRequest = new XMLHttpRequest();
// booksRequest.open('GET', 'https://www.googleapis.com/books/v1/volumes?q=quilting')
// booksRequest.onload = function(){
//   var ourBooksData = JSON.parse(booksRequest.responseText);
//     console.log(ourBooksData[0]);
// };
// booksRequest.send();


//  13 nov 2018
// $(document).ready(function(){
//   $.ajax({
//     method: "GET",
//     url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
//     success: function(hasil){
//       var books_list = hasil.items;
//       for (var i = 0; i < books_list.length; i++){
//         var title = books_list[i].volumeInfo.title;
//         var author = books_list[i].volumeInfo.authors[0];
//         var published = books_list[i].volumeInfo.publishedDate;
//         var temp = title.replace(/'/g, "\\'");
//         var table = 
//         '<tr class="table-books">'+
//         '<td class="title">'+title+'</td>'+
//         '<td class= "author">'+author+'</td>'+
//         '<td class= "published">' + published+'</td>'+
//         '<td class= "favourite">' + '<button class="btnnn" onClick="addToFavourite(\''+temp+'\')"><i class="far fa-star"></i></button>' + '</td>';
//         $('tbody').append(table);
//       }
//     },
//     error: function(error){
//       alert("Mohon maap gaada:(");
//     }
//   });
// });

// var addToFavourite = function (title){
//   var csrftoken = $("[name=csrfmiddlewaretoken]").val();
//   $.ajax({
//     method: "POST",
//     url: "/",
//     headers:{
//       "X-CSRFToken": csrftoken
//     },
//     data: {title: title},
//     success: function (hasil){
//       button = '<button class = "btnnn" onClick="deleteFromFavourite(\'' + hasil.id + '\')">' + '<i class="far fa-star active"></i></button>' + '</td>';
//       $(".btnnn").replaceWith(button);
//       $(".numbers").replaceWith('<p class="numbers" text-align = "right">'+ hasil.count +'</p>');
//     }
//   });
// };

// $(document).ready(function(){
$(document).on('click', '#searchbutton', function(){
  $('tbody').empty();
  var search_result = $("#search").val();  
$.ajax({
    url: "/Books/data?search=" + search_result,
    datatype: 'json',
    success: function(result){
        console.log("test");
        var books_object = jQuery.parseJSON(result);
        renderHTML(books_object);
    }
  });
})  

function renderHTML(data){
    console.log("render");
    var books_list = data.items;
          for (var i = 0; i < books_list.length; i++){
            var title = books_list[i].volumeInfo.title;
            var author = books_list[i].volumeInfo.authors[0];
            var published = books_list[i].volumeInfo.publishedDate;
            var temp = title.replace(/'/g, "\\'");
            var table = 
            '<tr class="table-books">'+
            '<td class = "nomor">' + (i+1) + '</td>' +
            '<td class = "image">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
            '<td class="title">'+title+'</td>'+
            '<td class= "author">'+author+'</td>'+
            '<td class= "published">' + published+'</td>'+
            '<td class= "favourite">' + '<button id = "star" ><i class = "fas fa-star"</i></button>' + '</td>';
            $('tbody').append(table);
          }
  }



var counter = 0;

$(function(){
  $(document).on('click', '#star', function(){
    if($(this).hasClass('favorited')){
      counter -= 1;
      $(this).removeClass('favorited');
      $(this).css("color","white");
    }

    else{
      counter +=1;
      $(this).addClass('favorited');
      $(this).css("color", "#631731");
    }

    $(".numbers").html(counter);
  });
});










