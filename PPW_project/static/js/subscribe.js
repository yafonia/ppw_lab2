var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

function validate_email(){
    var name = $("#name_input").val();
    var email = $("#email_input").val();
    var password = $("#password_input").val();
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").attr('value');
    $.ajax({
        url: '/PPW_app/validate/',
        method: 'POST', 
        datatype: 'json',
        data: JSON.stringify({'name' : name, 'email' : email, 'password' : password}),
        beforeSend: function(result){
            result.setRequestHeader('X-CSRFToken', csrftoken);    
        },

        success: function(result){
            var email_is_exist = result.email_is_exist;
            var form_is_valid = result.form_is_valid;
            console.log(email_is_exist)
            console.log(form_is_valid)
            if (email_is_exist || !form_is_valid){
                $("#submit_btn").prop("disabled", true);
                $("#info").prop("hidden", false);

                if (!form_is_valid){
                    $("#info").text("Your form is not valid!");
                }

                else if (email_is_exist){
                    $("#info").text("This email already exists.");
                }
            }

            else{
                console.log("halo")
                $("#submit_btn").prop("disabled", false);
                $("#info").text("Submit!");
            }
        }

     });
}

function postForm(){
    var name = $("#name_input").val();
    var email = $("#email_input").val();
    var password = $("#password_input").val();
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").attr('value');
    $.ajax({
        url: '/PPW_app/subscribe/',
        method: 'POST', 
        datatype: 'json',
        data: JSON.stringify({'name' : name, 'email' : email, 'password' : password}),
        beforeSend: function(result){
            result.setRequestHeader('X-CSRFToken', csrftoken);    
        },
        success: function(result){
            var form_is_posted = result.form_is_posted;
            
            if (form_is_posted){
                $("#submit_btn").prop("disabled", true);
                $("#info").prop("hidden", false);
                $("#info").text("Thanks for the subscribe!");
            }
        }
})
getAll();
}

function getAll(){
    $.ajax({
        url: '/PPW_app/subscriber_objects/',
        method: 'GET',
        dataType: 'json',
        success: function(result){
            $('tbody').empty();
            $.each(result, function (i, subscriber_list){
                var table =
                '<tr class="table-books">'+
                '<td class = "nomor">' + (i+1) + '</td>' +
                '<td class = "name">'  + (subscriber_list['fields']['name']) +  '</td>'+
                '<td class="email">' + (subscriber_list['fields']['email']) + '</td>' + 
                '<td class = "subscribe">' + '<button onclick = "unsubscribe(' + subscriber_list["pk"] + ')" type="button" class="btn">Unsubscribe</button>' + '</td>' + '</tr>';
                $('tbody').append(table);
            });
        
        }
    })
}


getAll();

function unsubscribe(pk){
    $.ajax({
        url: '/PPW_app/unsubscribe/',
        method: 'POST',
        data: {'pk':pk},
        dataType: 'json',
        success: function(){
            getAll();
            alert("Unsubscribed!")
        }
    })
}
