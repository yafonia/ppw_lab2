"""PPW_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
import PPW_app.urls as PPW_app
import Hello.urls as Hello
import MyProfile.urls as MyProfile
import Books.urls as Books
from PPW_app import views
from Hello import views
from MyProfile import views
from Books import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('PPW_app/', include(PPW_app)),
    # path('Hello/', include(Hello)),
    path('', include(Hello)),
    path('MyProfile/', include(MyProfile)),
    path('Books/', include(Books)),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
