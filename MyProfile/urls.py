from django.conf.urls import url
from .views import myProfile

urlpatterns = [
    url(r'^$', myProfile, name = 'myProfile')
]
