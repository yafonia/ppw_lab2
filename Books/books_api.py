import requests

books_api_url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
def get_books():
    return requests.get(books_api_url).json()