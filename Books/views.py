from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import logout as logout_user
import json
import requests




def data(request):
    try: 
        search = request.GET["search"]
    except:
        search = "quilting"

    getBooksJson = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + search)   
    jsonParsed = json.dumps(getBooksJson.json())
    return HttpResponse(jsonParsed)    

def index(request):
    # if request.user.is_authenticated:
    #     if "liked" not in request.session:
    #         request.session["full_name"] = request.user.first_name + " " + request.user.last_name
    #         request.session["username"] = request.user.username
    #         request.session["email"] = request.user.email
    #         request.session["sessionid"] = request.session.session_key
    #         request.session["liked"] = []
        
    return render(request, "books.html")

def login(request):
    return render (request, "login.html")

def logout(request):
    logout_user(request)
    return redirect("login.html")





# Create your views here.
