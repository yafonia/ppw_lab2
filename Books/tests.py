from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import data
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time
# Create your tests here.
class Story6UnitTest(TestCase):

    def test_story_9_url_is_exist(self):
        response = Client().get('/Books/')
        self.assertEqual(response.status_code,200)

    def test_story_9_hello_template(self):
        response = Client().get('/Books/')
        self.assertTemplateUsed(response, 'books.html')
    
    def test_story_9_using_index_func(self):
        found = resolve('/Books/')
        self.assertEqual(found.func, index)
    
    def test_story_9_using_data_func(self):
        found = resolve('/Books/data/')
        self.assertEqual(found.func, data)


    
    
class Story9FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story9FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Story9FunctionalTest, self).tearDown()



    def test_title(self):
        self.browser.get('http://ppw-c-yafonia.herokuapp.com/Books/')
        # self.browser.get('http://localhost:8000/Books/')
        time.sleep(5)
        self.assertIn("Books", self.browser.title)
        

    def test_header(self):
        self.browser.get('http://ppw-c-yafonia.herokuapp.com/Books/')
        # self.browser.get('http://localhost:8000/Books/')
        header_text = self.browser.find_element_by_class_name('table-heading').text
        time.sleep(5)
        self.assertIn("Highlight of the month", header_text)
    
    def test_header_with_css_property(self):
        self.browser.get('http://ppw-c-yafonia.herokuapp.com/Books/')
        # self.browser.get('http://localhost:8000/Books/')
        header_text = self.browser.find_element_by_class_name('table-heading').value_of_css_property('text-align')
        time.sleep(5)
        self.assertIn('center', header_text)
    



