from django.conf.urls import url, include
from django.contrib.auth import views
from .views import index
from .views import data
from .views import logout
from django.conf import settings

urlpatterns = [
   url(r'^$', index, name='index'),
   url(r'^data/$', data, name='databooks'),
   url(r'^auth/', include('social_django.urls', namespace='social')),
   url(r'^login/$', views.LoginView.as_view() , name ='login'),
   url(r'^logout/$', views.LogoutView.as_view(), name ='logout'),
]