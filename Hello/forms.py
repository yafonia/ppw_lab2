from django import forms
from django.forms import ModelForm
from .models import Status

class Status_Form(forms.ModelForm):
   class Meta:
       model = Status
       fields = ["status"]
       widgets = {
           "status" : forms.TextInput(attrs={'class' : 'form-control'}),
           }