from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

# Create your views here.

response = {}
def index(request):
    form = Status_Form(request.POST or None)
    
    # response['status'] = Status.objects.all()
    # html = "hello.html"
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        statusku = Status(status=response['status'])
        statusku.save()
        return HttpResponseRedirect('/Hello/')
    else:
        context = {
            'stat': Status.objects.all(),
        }
    return render(request,'hello.html', {'form': Status_Form(), 'stat':Status.objects.all()})


# def add_status(request):
#     form = Status_Form(request.POST or None)
#     if(request.method == "POST"):
#         response["status"] = request.POST["status"]
 
#         status_pribadi = Status(status = response["status"]
#         status_pribadi.save()
#         html = "hello.html"
#         jadwal = Status.objects.all()
#         response['status'] = status
#         return render(request, html, response)

#     else:
#         return HttpResponseRedirect('/')

# def status_response(request):
#     response["status"] = Status.objects.all().values()
#     return render(request,"hello.html",response)
# def add_status(request):
#     form = Status_Form(request.POST or None)
#     response = {}
#     if(request.method == 'POST' and form.is_valid()):
#         response['title'] = request.POST['title']
#         status = Status(title=response['title'])
#         status.save()
#         return HttpResponseRedirect('/Hello/')
#     else:
#         return HttpResponseRedirect('/Hello/')


