# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from .views import index
# from .models import Status
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import unittest
# import time
# # Create your tests here.
# class Story6UnitTest(TestCase):

#     def test_story_6_url_is_exist(self):
#         response = Client().get('/Hello/')
#         self.assertEqual(response.status_code,200)

#     def test_story_6_hello_template(self):
#         response = Client().get('/Hello/')
#         self.assertTemplateUsed(response, 'hello.html')
    
#     def test_story_6_using_index_func(self):
#         found = resolve('/Hello/')
#         self.assertEqual(found.func, index)
    
#     def test_landing_page_(self):
#         response = Client().get('/')
#         html_response = response.content.decode('utf-8')
#         self.assertIn("Hello! How's your day?",html_response)

#     def test_model_can_create_new_status(self):
#         new_status = Status.objects.create(status= 'create new status')
#         counting_all_available_status = Status.objects.all().count()
#         self.assertEqual(counting_all_available_status, 1)
    
#     # def test_form_validation_for_blank_items(self):
#     #     form_invalid = Status_Form(data={'status': ''})
#     #     self.assertFalse(form_invalid.is_valid())
#     #     self.assertEqual(
#     #         form_invalid.errors['status'],
#     #         ["This field is required."]
#     #     )
    
#     def test_Story6_post_success_and_render_the_result(self):
#         test = 'Anonymous'
#         response_post = Client().post('/Hello/', {'status' : test})
#         self.assertEqual(response_post.status_code,302)

#         response = Client().get('/Hello/')
#         html_response = response.content.decode('utf8')
#         self.assertIn(test,html_response)
    
#     def test_create_anything_model(self, judul ='ini judul'):
#         return Status.objects.create(status=judul)
    
    
# class Story6FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Story6FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(Story6FunctionalTest, self).tearDown()


    
#     def test_input_status(self):
#         self.browser.get('http://ppw-c-yafonia.herokuapp.com/Hello/')
#         # self.browser.get('http://localhost:8000/Hello/')
#         time.sleep(5) 
#         search_box = self.browser.find_element_by_class_name('form-control')
#         search_box.send_keys('Coba Coba')
#         search_box.submit()
#         time.sleep(5)
#         self.assertIn( "Coba Coba", self.browser.page_source)


#     def test_title(self):
#         self.browser.get('http://ppw-c-yafonia.herokuapp.com/Hello/')
#         # self.browser.get('http://localhost:8000/Hello/')
#         time.sleep(5)
#         self.assertIn("how's your day?", self.browser.title)
        

#     def test_header(self):
#         # self.browser.get('http://localhost:8000/Hello/')
#         self.browser.get('http://ppw-c-yafonia.herokuapp.com/Hello/')
#         header_text = self.browser.find_element_by_tag_name('p').text
#         time.sleep(5)
#         self.assertIn("Hello! How's your day?", header_text)
    
#     def test_header_with_css_property(self):
#         self.browser.get('http://ppw-c-yafonia.herokuapp.com/Hello/')
#         header_text = self.browser.find_element_by_tag_name('p').value_of_css_property('text-align')
#         time.sleep(5)
#         self.assertIn('center', header_text)
    
#     def test_response_header_with_css_property(self):
#         self.browser.get('http://ppw-c-yafonia.herokuapp.com/Hello/')
#         header_text = self.browser.find_element_by_tag_name('p').value_of_css_property('font-size')
#         time.sleep(5)
#         self.assertIn('50px', header_text)

#     # def test_body_with_css_property(self):
#     #     self.browser.get('http://ppw-c-yafonia.herokuapp.com/Hello/')
#     #     body_tag = self.browser.find_element_by_tag_name('body').value_of_css_property('background-color')
#     #     time.sleep(5)
#     #     self.assertIn('rgba(255, 224, 112,1)', body_tag)


    
    
        

  

#     # def test_form_status_input_has_placeholder_and_css_classes(self):
#     #     form = Status_Form()
#     #     self.assertIn('class="status-form-input', form.as_p())
#     #     self.assertIn('id="id_title"', form.as_p())
#     #     self.assertIn('class="status-form-textarea', form.as_p())
#     #     self.assertIn('id="id_description', form.as_p())

