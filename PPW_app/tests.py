from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import data
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class ProfileUnitTest(TestCase):

    def test_home_url_is_exist(self):
        response = Client().get('/PPW_app/')
        self.assertEqual(response.status_code,200)
    

    def test_profile_url_is_exist(self):
        response = Client().get('/PPW_app/profile/')
        self.assertEqual(response.status_code,200)


    def test_contact_url_is_exist(self):
        response = Client().get('/PPW_app/contact/')
        self.assertEqual(response.status_code,200)


    def test_schedule_submission_url_is_exist(self):
        response = Client().get('/PPW_app/schedule_submission')
        self.assertEqual(response.status_code,200)


    def test_my_schedule_url_is_exist(self):
        response = Client().get('/PPW_app/my_schedule/')
        self.assertEqual(response.status_code,200)


    def test_post_jadwal_url_is_exist(self):
        response = Client().get('/PPW_app/post_jadwal/')
        self.assertEqual(response.status_code,200)


    def test_delete_schedule_url_is_exist(self):
        response = Client().get('/PPW_app/delete_response/')
        self.assertEqual(response.status_code,200)


    def test_subscribe_url_is_exist(self):
        response = Client().get('/PPW_app/subscribe/')
        self.assertEqual(response.status_code,200)


    def test_validate_url_is_exist(self):
        response = Client().get('/PPW_app/validate/')
        self.assertEqual(response.status_code,200)

    def test_subscriber_objects_url_is_exist(self):
        response = Client().get('/PPW_app/subscriber_objects/')
        self.assertEqual(response.status_code,200)

    def test_unsubscribe_url_is_exist(self):
        response = Client().get('/PPW_app/unsubscribe/')
        self.assertEqual(response.status_code,200)

    def test_home_template(self):
        response = Client().get('/PPW_app/')
        self.assertTemplateUsed(response, 'story3.html')

    def test_profile_template(self):
        response = Client().get('/PPW_app/profile/')
        self.assertTemplateUsed(response, 'profile.html')
    
    def test_contact_template(self):
        response = Client().get('/PPW_app/contact/')
        self.assertTemplateUsed(response, 'contact.html')

    def test_schedule_submission_template(self):
        response = Client().get('/PPW_app/schedule_submission/')
        self.assertTemplateUsed(response, 'schedule_submission.html')

    def test_my_schedule_template(self):
        response = Client().get('/PPW_app/my_schedule/')
        self.assertTemplateUsed(response, 'my_schedule.html')

    def test_jadwal_response_template(self):
        response = Client().get('/PPW_app/my_schedule/')
        self.assertTemplateUsed(response, 'my_schedule.html')

    def test_delete_response_template(self):
        response = Client().get('/PPW_app/delete_response/')
        self.assertTemplateUsed(response, 'my_schedule.html')

    def test_subscribe_template(self):
        response = Client().get('/PPW_app/subscribe/')
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_subscribe_template(self):
        response = Client().get('/PPW_app/subscriber_objects/')
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_subscribe_template(self):
        response = Client().get('/PPW_app/unsubscribe/')
        self.assertTemplateUsed(response, 'subscribe.html')


    def test_home_using_index_func(self):
        found = resolve('/PPW_app/')
        self.assertEqual(found.func, tes)

    def test_profile_using_index_func(self):
        found = resolve('/PPW_app/profile/')
        self.assertEqual(found.func, tes2)

    def test_contact_using_index_func(self):
        found = resolve('/PPW_app/contact/')
        self.assertEqual(found.func, tes3)

    def test_schedule_submission_using_index_func(self):
        found = resolve('/PPW_app/schedule_submission/')
        self.assertEqual(found.func, jadwal_submission)

    def test_my_schedule_using_index_func(self):
        found = resolve('/PPW_app/my_schedule/')
        self.assertEqual(found.func, post_jadwal)
 
    def test_schedule_response_using_index_func(self):
        found = resolve('/PPW_app/my_schedule/')
        self.assertEqual(found.func, jadwal_response)

    def test_delete_response_using_index_func(self):
        found = resolve('/PPW_app/my_schedule/')
        self.assertEqual(found.func, delete_response)

    def test_subscribe_using_index_func(self):
        found = resolve('/PPW_app/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_validate_using_index_func(self):
        found = resolve('/PPW_app/validate/')
        self.assertEqual(found.func, validate)

    def test_subscribe_objects_using_index_func(self):
        found = resolve('/PPW_app/subscriber_objects/')
        self.assertEqual(found.func, subscriber_objects)

    def test_unsubscribe_using_index_func(self):
        found = resolve('/PPW_app/unsubscribe/')
        self.assertEqual(found.func, unsubscribe)



    def test_model_can_create_new_status(self):
        new_status = Subscribe.objects.create(name= 'create new status', email = 'lalala@gmail.com', password='la')
        counting_all_available_status = Subscribe.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

# Create your tests here.
