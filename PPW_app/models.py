from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Jadwal(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    hari_kegiatan = models.CharField(max_length=30)
    tanggal_kegiatan = models.DateField()
    waktu_kegiatan = models.TimeField()
    tempat_kegiatan = models.CharField(max_length=30)
    kategori_kegiatan = models.CharField(max_length=30)

class Subscribe(models.Model):
    name = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    password = models.CharField(max_length=30)

    # def as_dict(self):
    #     return{
    #         "name": self.name,
    #         "email": self.email,
    #         "password": self.password
    #     }
    
# Create your models here.
