from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import Message_Form
from .forms import Subscribe_Form
from .models import Jadwal
from .models import Subscribe
import json
from django.core import serializers




response = {'author' : 'Yafonia Hutabarat'}

def tes(request):
    return render(request,"story3.html")

def tes2(request):
    return render(request,"profile.html")

def tes3(request):
    return render(request,"contact.html")

def subscribe(request):
    return render(request, "subscribe.html")

def jadwal_submission(request):
    html = "schedule_submission.html"
    response["message_form"] = Message_Form
    return render(request, html, response)

def post_jadwal(request):
    form = Message_Form(request.POST or None)
    if(request.method == "POST"):
        response["nama_kegiatan"] = request.POST["nama_kegiatan"]
        response["hari_kegiatan"] = request.POST["hari_kegiatan"]
        response["tanggal_kegiatan"] = request.POST["tanggal_kegiatan"]
        response["waktu_kegiatan"] = request.POST["waktu_kegiatan"]
        response["tempat_kegiatan"] = request.POST["tempat_kegiatan"]
        response["kategori_kegiatan"] = request.POST["kategori_kegiatan"]

        jadwal_pribadi = Jadwal(nama_kegiatan = response["nama_kegiatan"], hari_kegiatan = response["hari_kegiatan"], tanggal_kegiatan = response["tanggal_kegiatan"], waktu_kegiatan = response["waktu_kegiatan"], tempat_kegiatan = response["tempat_kegiatan"], kategori_kegiatan = response["kategori_kegiatan"])
        jadwal_pribadi.save()
        html = "my_schedule.html"
        jadwal = Jadwal.objects.all()
        response['jadwal'] = jadwal
        return render(request, html, response)

    else:
        return HttpResponseRedirect('/')

def jadwal_response(request):
    response["jadwal"] = Jadwal.objects.all().values()
    return render(request,"my_schedule.html",response)

def delete_response(request):
    jadwal  = Jadwal.objects.all().delete()
    response["jadwal"] = jadwal
    return render(request,"my_schedule.html", response)


def subscribe(request):
    response ["subscribe_form"] = Subscribe_Form
    form = Subscribe_Form(request.POST or None)
    if (request.method == "POST"and request.is_ajax()):
        request = json.loads(request.body)

        subscriber = Subscribe(name=request['name'], email=request['email'], password = request['password'])
        subscriber.save()
       
        return JsonResponse({'form_is_posted' : True})
        

    else:
        return render(request, "subscribe.html", response)

def validate(request):

    if (request.method == "POST" and request.is_ajax()):
        request = json.loads(request.body)
        email_is_exist = False
        form_is_valid = False
        form = Subscribe_Form({'name' : request['name'], 'email' : request['email'], 'password' : request['password']})

        if(Subscribe.objects.filter(email= request['email']).first()):
            email_is_exist = True
        
        if (form.is_valid()):
            form_is_valid = True
        
        return JsonResponse({'email_is_exist' : email_is_exist, 'form_is_valid' : form_is_valid})

def subscriber_objects(request):
    subscriber_list = Subscribe.objects.all()
    return HttpResponse(serializers.serialize('json', subscriber_list), content_type = 'application/json')

def unsubscribe(request):
    if (request.method == "POST" and request.is_ajax()):
        pk_subscriber = request.POST['pk']
        Subscribe.objects.filter(pk = pk_subscriber).delete()
        return JsonResponse({'unsubscribed' : True})




# @csrf_exempt
# def add_subscribe(request):
#     form = Subscribe_Form(request.POST or None)
#     if(request.method == 'POST' and form.is_valid()):
#         response['name'] = request.POST['name']
#         response['email'] = request.POST['email']
#         response['password'] = request.POST['password']
#         subscriber = Subscribe(name=response['name'], email=response['email'], password = response['password'])
#         subscriber.save()
#         return HttpResponseRedirect('/')
#     else:
#         response = {
#             'subscriber_objects': Subscribe.objects.all(),
#         }
#     return render(request,'subscribe.html', {'form': Subscribe_Form(), 'subscriber_objects' : Subscribe.objects.all()})
   
# @csrf_exempt
# def validate_email(request):
#     email = request.POST.get('email', None)
#     data = {
#         'is_taken': Subscribe.objects.filer(email=email).exists()
#     }

#     return HttpResponseRedirect('/')
    

