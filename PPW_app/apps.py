from django.apps import AppConfig


class PpwAppConfig(AppConfig):
    name = 'PPW_app'
