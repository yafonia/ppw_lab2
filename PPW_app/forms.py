from django import forms

class Message_Form(forms.Form):

    # attrs = {
    #     'class': 'form-control'
    # }
    
    nama_kegiatan = forms.CharField(label = "Name")
    hari_kegiatan = forms.CharField(label = "Day")
    tanggal_kegiatan = forms.DateField(label = "Date", widget = forms.DateInput(attrs={'type' : 'date'}))
    waktu_kegiatan = forms.TimeField(label = "Time", widget = forms.TimeInput(attrs={'type' : 'time'}))
    tempat_kegiatan = forms.CharField(label = "Venue")
    kategori_kegiatan = forms.CharField(label = "Category")

class Subscribe_Form(forms.Form):
    name = forms.CharField(label = "Name", required = True, widget = forms.TextInput(attrs={'id' : 'name_input', 'oninput' : 'validate_email()'}))
    email = forms.EmailField(label = "Email", required = True, widget = forms.EmailInput(attrs={'id' : 'email_input', 'oninput' : 'validate_email()'}))
    password = forms.CharField(label = "Password", widget=forms.PasswordInput(attrs={'id' : 'password_input', 'oninput' : 'validate_email()'}), required = True)


    # nama_kegiatan = forms.CharField(label = "Name", required=True,
    # max_length=27, empty_value='The event has no name',
    # widget=forms.TextInput(attrs=attrs))

    # waktu_kegiatan = forms.DateTimeField(required=True,
    # widget=forms.DateTimeInput(attrs=attrs))

    # tempat_kegiatan = forms.CharField(label = "Venue", required=True,
    # max_length=27, empty_value='The event has no venue',
    # widget=forms.TextInput(attrs=attrs))

    # kategori_kegiatan = forms.CharField(label = "Category", required=True,
    # max_length=27, empty_value='The event has no category',
    # widget=forms.TextInput(attrs=attrs))

