from django.conf.urls import url
from .views import tes
from .views import tes2
from .views import tes3
from .views import jadwal_submission
from .views import jadwal_response
from .views import post_jadwal
from .views import delete_response
from .views import subscribe
from .views import validate
from .views import subscriber_objects
from .views import unsubscribe

# from .views import add_subscribe


urlpatterns = [
    url(r'^$', tes, name='tes'),
    url(r'^profile/', tes2, name='tes2'),
    url(r'^contact/', tes3, name='tes3'),
    url(r'^schedule_submission/', jadwal_submission, name = 'jadwal_submission'),
    url(r'^my_schedule/', jadwal_response, name = 'jadwal_response'),
    url(r'^post_jadwal/', post_jadwal, name = 'post_jadwal'),
    url(r'^delete_response/', delete_response, name = 'delete_response'),
    url(r'^subscribe/', subscribe, name = 'subscribe'),
    url(r'^validate/', validate, name = 'validate'),
    url(r'^subscriber_objects/', subscriber_objects, name = 'subscriber_objects'),
    url(r'^unsubscribe/', unsubscribe, name = 'unsubscribe')
    
    # url(r'^add_subscribe/', add_subscribe, name = 'add_subscribe')
  
]
